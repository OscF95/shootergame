using UnityEngine;

public class SoldierDamageController : MonoBehaviour
{
    private SoldierController soldier;

    private void Start()
    {
        soldier = transform.parent.gameObject.GetComponent<SoldierController>();
    }

    private void OnTriggerEnter(Collider other)
    {

        if (other.gameObject.CompareTag("Arrow"))
        {
            arrowDamage();
        }
        else if (other.gameObject.CompareTag("Bullet"))
        {
            bulletDamage();
        }
        else if (other.gameObject.CompareTag("RocketBullet")) 
        {
            reduceSoldierHealt(100);
        }
        else if (other.gameObject.CompareTag("Knife"))
        {
            reduceSoldierHealt(20);
        }
    }

    void arrowDamage() 
    {
        if (transform.gameObject.CompareTag("Head"))
        {
            reduceSoldierHealt(100);
        }
        else if (transform.gameObject.CompareTag("Body")) 
        {
            reduceSoldierHealt(60);
        }
        else if (transform.gameObject.CompareTag("Tip"))
        {
            reduceSoldierHealt(20);
        }
    }

    void bulletDamage() 
    {
        if (transform.gameObject.CompareTag("Head"))
        {
            reduceSoldierHealt(100);
        }
        else if (transform.gameObject.CompareTag("Body"))
        {
            reduceSoldierHealt(45);
        }
        else if (transform.gameObject.CompareTag("Tip"))
        {
            reduceSoldierHealt(15);
        }
    }

    void reduceSoldierHealt(int damage) 
    {
        soldier.healt = soldier.healt - damage;
    }
}
