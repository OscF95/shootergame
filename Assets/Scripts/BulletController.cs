using UnityEngine;

public class BulletController : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _timeToDestory;
    void Start()
    {
        Destroy(gameObject, _timeToDestory);
    }

    void Update()
    {
        transform.Translate(Vector3.forward * (_speed * Time.deltaTime));
    }
}
