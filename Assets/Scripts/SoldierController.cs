using TMPro;
using UnityEngine;

public class SoldierController : MonoBehaviour
{
    [SerializeField] private GameObject healtText;
    [SerializeField] private float timeToCreateNewSoldier;
    public int healt = 100;
    private string soldierHealtText;
    private GameObject newSoldier;
    private bool Killed;

    private void Start()
    {
        newSoldier = Instantiate(transform.gameObject);
        newSoldier.SetActive(false);
    }

    private void Update()
    {
        soldierHealtText = (healt <= 0) ? "0" : healt.ToString();
        healtText.GetComponent<TextMeshProUGUI>().text = string.Concat("Enemy Healt:", soldierHealtText);
        if (healt <= 0 && !Killed)
        {
            transform.gameObject.SetActive(false);
            Invoke("ActiveNewSoldier", 5f);
            Killed = true;
        }
    }

    private void ActiveNewSoldier()
    {
        newSoldier.SetActive(true);
        Destroy(transform.gameObject);
    }
}
