using System;
using System.Threading;
using UnityEngine;

public class WeaponShooterController : MonoBehaviour
{
    [SerializeField] private AudioClip _shootSound;
    [SerializeField] private GameObject _bullet;
    [SerializeField] private string _bulletTag;
    private Vector3 currentBulletPosition;
    private Vector3 currentBulletAngle;
    private ObjectsHandler objectsHandler = new ObjectsHandler();

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            GameObject bulletObject = objectsHandler.GetObjectByTag(transform, _bulletTag);
            currentBulletPosition = bulletObject.transform.position;
            currentBulletAngle = bulletObject.transform.eulerAngles;

            AlreadyCreatedBullet forwardBullet = bulletObject.GetComponent<AlreadyCreatedBullet>();
            forwardBullet.shooted = true;
            bulletObject.transform.SetParent(transform.parent);
            AudioSource.PlayClipAtPoint(_shootSound, transform.position);
            LoadBullet(currentBulletPosition, currentBulletAngle);
        }
    }

    void LoadBullet(Vector3 objectPosition, Vector3 objectAngle)
    {
        Vector3 originalScale = _bullet.transform.localScale;
        GameObject newObject = Instantiate(_bullet, objectPosition, Quaternion.Euler(objectAngle));
        newObject.transform.SetParent(transform);
        if (newObject.CompareTag("Arrow"))
        {
            newObject.transform.localScale = originalScale;
        }
    }
}
