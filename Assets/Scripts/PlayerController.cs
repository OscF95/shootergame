using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [Header("Player Settings")]
    [SerializeField] Camera _playerCamera;
    [SerializeField] private float _speed;
    [SerializeField] private float _rotationSensibility;
    [Space(10)]
    [Header("Weapons")]
    public GameObject[] weapons;
    private string[] availableWeapons = new string[3];
    private int weaponNumber = 0;

    private Vector3 rotationInput = Vector3.zero;
    private float cameraVerticalAngle;

    void Update()
    {
        Move();
        Look();
        ChooseWeapon();
    }

    private void Move()
    {
        transform.Translate(Vector3.forward * Input.GetAxis("Vertical") * _speed * Time.deltaTime);
        transform.Translate(Vector3.right * Input.GetAxis("Horizontal") * _speed * Time.deltaTime);
    }

    private void Look()
    {
        rotationInput.x = Input.GetAxis("Mouse X") * _rotationSensibility * Time.deltaTime;
        rotationInput.y = Input.GetAxis("Mouse Y") * _rotationSensibility * Time.deltaTime;

        cameraVerticalAngle += rotationInput.y;
        cameraVerticalAngle = Mathf.Clamp(cameraVerticalAngle, -70, 70);

        transform.Rotate(Vector3.up * rotationInput.x);
        _playerCamera.transform.localRotation = Quaternion.Euler(-cameraVerticalAngle, 0f, 0f);
    }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Rifle") || other.gameObject.CompareTag("Bow") || other.gameObject.CompareTag("Rocket")) 
        {
            availableWeapons[weaponNumber] = other.gameObject.tag;
            Destroy(other.gameObject);
            weaponNumber++;
        }
    }

    private void ChooseWeapon()
    {
        if (Input.GetKeyDown("1"))
        {
            ActiveWeapon(0);
        } else if (Input.GetKeyDown("2"))
        {
            ActiveWeapon(1);
        }
        else if (Input.GetKeyDown("3"))
        {
            ActiveWeapon(2);
        }
        else if (Input.GetKeyDown("4")) 
        {
            ActiveWeapon(3);
        }
    }

    private void ActiveWeapon(int option)
    {
        
        if (option != 3)
        {
            for (int i = 0; i < weapons.Length; i++)
            {
                if (string.Equals(weapons[i].tag, availableWeapons[option]))
                {
                    UnactiveCurrentWeapon();
                    weapons[i].SetActive(true);
                    break;
                }
            }
        }
        else 
        {
            UnactiveCurrentWeapon();
            weapons[option].SetActive(true);
        }
    }

    private void UnactiveCurrentWeapon() 
    {
        Transform weaponParent = transform.GetChild(0);
        GameObject currentGameObject;
        for (int i = 0; i < weaponParent.childCount; i++) 
        {
            currentGameObject = weaponParent.GetChild(i).gameObject;

            if (currentGameObject.activeSelf) 
            {
                currentGameObject.SetActive(false);
            }
        }
    }
}
