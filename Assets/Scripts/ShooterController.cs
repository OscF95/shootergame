using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShooterController : MonoBehaviour
{
    [Header("Files")]
    [SerializeField] private GameObject _bulletPrefab;
    [SerializeField] private AudioClip _shootSound;
    [SerializeField] private AudioClip _reloadSound;

    [Header("Settings")]
    [SerializeField] private float _frequency;
    [SerializeField] private float _magazineSize;
    [Space(10)]
    [Tooltip("Activate this parameter for gun autoreload")] [SerializeField] private bool _autoReload;
    [SerializeField] private float _autoReloadTime;

    private float _timeLastBullet;
    private float _magazineSizeInitialValue;

    void Awake()
    {
        _magazineSizeInitialValue = _magazineSize;
    }

    private void Update()
    {
        if (Input.GetButtonDown("Fire1") && _magazineSize > 0)
        {
            if (Time.time - _timeLastBullet > _frequency)
            {
                Shoot();
                _timeLastBullet = Time.time;
            }
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            Invoke(nameof(PlayReloadSound), _autoReloadTime / 2);
            Reload();
        }
    }

    void Shoot()
    {
        Instantiate(_bulletPrefab, transform.position, Quaternion.Euler(transform.eulerAngles));
        AudioSource.PlayClipAtPoint(_shootSound, transform.position);
        _magazineSize--;

        if (_magazineSize <= 0 && _autoReload)
        {
            Invoke(nameof(PlayReloadSound), _autoReloadTime / 2);
            Invoke(nameof(Reload), _autoReloadTime);
        }

    }

    void PlayReloadSound()
    {
        AudioSource.PlayClipAtPoint(_reloadSound, transform.parent.position);
    }

    void Reload()
    {
        _magazineSize = _magazineSizeInitialValue;
    }
}
