using UnityEngine;

public class AlreadyCreatedBullet : MonoBehaviour
{
    [SerializeField] private float _speed;
    [SerializeField] private float _timeToDestroy;
    [SerializeField] private bool _isForward;
    public bool shooted = false;
    private Vector3 direction;

    void Update()
    {
        if (shooted) 
        {
            direction = _isForward ? Vector3.forward : Vector3.back;
            transform.Translate(direction * (_speed * Time.deltaTime));
            DestroyCurrentBullet();
        }
    }

    void DestroyCurrentBullet() 
    {
        Destroy(gameObject, _timeToDestroy);
    }
}
