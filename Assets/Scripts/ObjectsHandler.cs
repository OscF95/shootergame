using UnityEngine;

public class ObjectsHandler
{
    public GameObject GetObjectByTag(Transform mainObject, string tag) 
    {
        Transform currentObject;
        for (int i=0; i < mainObject.childCount; i++) 
        {
            currentObject = mainObject.GetChild(i);
            if (currentObject.CompareTag(tag))
            {
                return currentObject.gameObject;
            }
        }
        return null;
    }
}
